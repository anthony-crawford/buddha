/**
 * LISCENCE
 */
var buddha;
(function(buddha){

    //Private Variables and Functions
    var EventMgmt = {
                            addEvent: function(node,event,handler){
                                if(node.addEventListener){
                                    node.addEventListener(event,handler,false);
                                }else if(node.attachEvent){
                                    node.attachEvent("on"+event,handler);
                                }else{
                                    node["on"+event] = handler;
                                }
                            },
                            
                            deleteEvent: function(node,event,handler){
                                if(node.removeEventListener){
                                    node.removeEventListener(event,handler,false);
                                }else if(node.detachEvent){
                                    node.detachEvent(event,handler);
                                }else{
                                    node["on"+event] = null;
                                }
                            }
                        }
    
    //Singleton Containing Applications Listing of Objects
    var applicationShare = function(){
       var applications =[];
        return {
                    addApplication: function(appName,appInstance){
                                                        var object =new Object();
                                                        object[appName] = appInstance;
                                                        applications.push(object)
                                                        //console.log(applications)
                                                    },
                    getApplications: function(){
                                                    //console.log(applications)
                                                    return applications;
                                                }
                }
    }();    
    
    //Application Constructor
    var bodhi = function(appName,dependencies){
           // console.log(dependencies)
            if(dependencies && (dependencies instanceof Array) && dependencies.length  > 0){
                this.dependencies={};
                for(var i=0,len = dependencies.length;i<len;i++){
                    this.dependencies[dependencies[i].valueOf()] = dependencies[i]
                }
                i=null;len=null;
            }
           
            this.appName = appName;
            applicationShare.addApplication(this.appName,this)
            return this
    };
    
    //Get List of other Applications that are on the page
    bodhi.prototype.getApplications = function(){
        this.listOfApplications = applicationShare.getApplications();
        return this;
    }
    
    //AJAX
    bodhi.prototype.request = function(config){
        if(config.method && config.url){
            var request = new XMLHttpRequest();
            
            //Success
            if(config.success){request.onload = function(){
                                    if(request.status >=200 && request.status <400){ 
                                        config.success(request.response);
                                    }else{
                                        if(!config.error){
                                            console.log("Request Failed: ", request.response);
                                        }
                                    } 
                                }  
            }; 
            
            //Error
            if(config.error){request.onerror = config.error(request.response)}
            
            //Open Connection
            request.open(config.method,config.url,true);
            
            //Configure Content-Type and Send Data
            if(config.method.toLowerCase() == ("post"||"put") ){
                if(config.contentType){
                    request.setRequestHeader('Content-Type', config.contentType);  
                    request.send(JSON.stringify(config.data));
                    
                }else{
                    request.setRequestHeader('Content-Type', "application/json");  
                    request.send(JSON.stringify(config.data));
                }
                      
            }else{
                //Using Get or Delete
                request.send(config.data);
            }
            
            
        }else{
            console.log("Missing Method or URL for Request")
        }
    
    }
    
    //Router
    bodhi.prototype.dharma = function(config){
        this.config = config;
        if(this.config && typeof this.config == "object" && Object.keys(this.config).length >0 ){
            if(this.config.verdas 
                && typeof this.config.verdas == "object" 
                    && Object.keys(this.config.verdas).length >0 
                        && this.dependencies && this.dependencies.router){
                        
                        if(this.routeMgmt){ this.routeMgmt.clearHandlers(["load","hashchange"]); }
                        this.routeMgmt = new this.dependencies.router(this).bootstrap(["load","hashchange"]);                
            }      
            
        
        }else{
            throw "Missing configuration settings!";
        }
        //console.log("dharma : Place Routes here for Frameworks ", this.appName);
        return this;
    }
    
    //Promise
    bodhi.prototype.promise = function(fn) {
                                          var callback = null;
                                          this.then = function(cb) {
                                            callback = cb;
                                          };

                                          function resolve(value) {
                                            // force callback to be called in the next
                                            // iteration of the event loop, giving
                                            // callback a chance to be set by then()
                                            setTimeout(function() {
                                              callback(value);
                                            }, 1);
                                          }

                                          fn(resolve);
                                      }
    
    
    buddha.bodhi = function(appName,dependencies){ return new bodhi(appName,dependencies)};
    
    

})(buddha||(buddha={}))
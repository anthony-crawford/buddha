    function Routing(app){
        this.app = app;
        this.routes = this.app.config.verdas;
        return this;
    }

    Routing.valueOf = function (){return "router";};

    Routing.prototype.eventMgmt = {
                            addEvent: function(node,event,handler){
                                if(node.addEventListener){
                                    node.addEventListener(event,handler,false);
                                }else if(node.attachEvent){
                                    node.attachEvent("on"+event,handler);
                                }else{
                                    node["on"+event] = handler;
                                }
                            },
                            
                            deleteEvent: function(node,event,handler){
                                if(node.removeEventListener){
                                    node.removeEventListener(event,handler,false);
                                }else if(node.detachEvent){
                                    node.detachEvent(event,handler);
                                }else{
                                    node["on"+event] = null;
                                }
                            }
                        }
    
    Routing.prototype.routingHashHandler = function(event){
        
        if(location.hash in this.routes){
           if(this.routes[location.hash].phenomenon){
             var view =   ("dukkha" in this.routes[location.hash].phenomenon ? "dukkha" : null) || ("anitya" in this.routes[location.hash].phenomenon ? "anitya" : null) || ("anatman" in this.routes[location.hash].phenomenon ? "anatman" : null);
               
               switch(view){
                    case "anatman":
                           alert("anatman");
                            this.routes[location.hash].tenet();
                    break;
                    case "dukkha":
                          
                            var that=this;
                            this.routes[location.hash].tenet().then(function(value){
                                   // console.log(value)
                                    var extension;
                                    if(that.app.config.dukkhaSufferType){ extension = that.app.config.dukkhaSufferType}else{ extension = ".html";}
                                        that.app.dependencies.renderer.templateCompiler(
                                                                that.routes[location.hash].phenomenon[view], 
                                                                JSON.parse(value), 
                                                                extension );
                            });
                           
                    break;
                    case "anitya":
                           alert("anitya");
                            this.routes[location.hash].tenet();
                    break;
                   default:
                        alert("Nada");
                   break;
               }
           
           }
            
        }else if(!location.hash){
            location.hash="#/";
        }else{
            if(this.routes.default){
                this.routes.default.tenet();
            }else{
                alert("404");
            }
            
        }
        
        //return this;
    
    }
    
    Routing.prototype.bootstrap = function(eventTypes,request){
        this.request = request;
        var that = this;
            for(var i=0, len =eventTypes.length; i<len;i++ ){
                this.eventMgmt.addEvent(window,eventTypes[i],function(event){that.routingHashHandler(event,that.request);} )
            }            
            return this;
    
    }
    
    Routing.prototype.clearHandlers = function(eventTypes,handler){
        for(var i=0, len =eventTypes.length; i<len;i++ ){
               window["on"+eventTypes[i]] = null;
            }            
            return this;
    }